//
//  ViewController.swift
//  ContainerVC
//
//  Created by mohammad.sheikh on 08/04/22.
//

import UIKit

class ViewController: UIViewController {

    let secondVC = SecondViewController()
    let thirdVC = ThirdViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setUp()
    }

    private func setUp() {
        
        
        addChild(secondVC)
        addChild(thirdVC)
        
        self.view.addSubview(secondVC.view)
        self.view.addSubview(thirdVC.view)
        
        secondVC.didMove(toParent: self)
        thirdVC.didMove(toParent: self)
        
        secondVC.view.frame = self.view.bounds
        thirdVC.view.frame = self.view.bounds
        
        thirdVC.view.isHidden = true
    }
    
    @IBAction func didTapSegment(Segment: UISegmentedControl){
        secondVC.view.isHidden = true
        thirdVC.view.isHidden = true
        if Segment.selectedSegmentIndex == 0 {
//            show red vc
            secondVC.view.isHidden = false
        } else {
//            show blue vc
            thirdVC.view.isHidden = false
        }
    }

}

